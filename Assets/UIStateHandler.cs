using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class UIStateHandler : MonoBehaviour
{
    public RenderTexture rTexture;
    public enum toolState
    {
        pen, eraser, delete
    };

    public toolState toolstate = toolState.pen;

    public void setPenActive()
    {
        toolstate = toolState.pen;
    }

    public void setEraserActive()
    {
        toolstate = toolState.eraser;
    }

    public void setDeleteActive()
    {
        toolstate = toolState.delete;
    }

    public void save()
    {
        StartCoroutine(saveCoroutine());
    }

    private IEnumerator saveCoroutine()
    {
        yield return new WaitForEndOfFrame();
        RenderTexture.active = rTexture;
        var outputTexture = new Texture2D(rTexture.width, rTexture.height);
        outputTexture.ReadPixels(new Rect(0, 0, rTexture.width, rTexture.height), 0, 0);
        outputTexture.Apply();

        //var bytes = outputTexture.EncodeToPNG();
        var bytes = outputTexture.EncodeToJPG();


        var dirPath = Application.dataPath + "/../SaveImages/";
        //var dirPath = "C:/Users/Asus/SaveImages/";

        if (!Directory.Exists(dirPath))
        {
            Debug.Log("created new folder");
            Directory.CreateDirectory(dirPath);
        }
        File.WriteAllBytes(dirPath + "Image" + ".jpeg", bytes);
        Debug.Log("Successfylly saved");
    }
}
